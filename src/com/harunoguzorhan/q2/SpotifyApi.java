package com.harunoguzorhan.q2;

import java.util.ArrayList;
import java.util.List;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.SettableFuture;
import com.wrapper.spotify.Api;
import com.wrapper.spotify.methods.AlbumRequest;
import com.wrapper.spotify.methods.TrackSearchRequest;
import com.wrapper.spotify.methods.authentication.ClientCredentialsGrantRequest;
import com.wrapper.spotify.models.Album;
import com.wrapper.spotify.models.ClientCredentials;
import com.wrapper.spotify.models.Page;
import com.wrapper.spotify.models.Track;

public class SpotifyApi {

	public static Api API = null;

	public static void auth() {
		API = Api.builder().clientId(AppProperties.SPOTIFY_CLIENT_ID)
				.clientSecret(AppProperties.SPOTIFY_SECRET_KEY)
				// .redirectURI("<your_redirect_uri>")
				.build();
	}

	public static List<Track> searchTracksByKeyword(String keyword) {
		final TrackSearchRequest request = API.searchTracks(keyword).build();
		try {
			final Page<Track> trackSearchResult = request.get();
//			System.out.println("I got " + trackSearchResult.getTotal()
//					+ " results!");
			return trackSearchResult.getItems();

		} catch (Exception e) {
			System.out.println("Something went wrong!" + e.getMessage());
		}
		return new ArrayList<Track>();
	}

}
