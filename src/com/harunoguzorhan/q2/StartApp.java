package com.harunoguzorhan.q2;

import com.wrapper.spotify.Api;

public class StartApp {

	public static void main(String[] args) {

		SpotifyApi.auth();
		AppProperties.loadProperties();
		
		AppFrame appFrame = new AppFrame();
		appFrame.pack();
		appFrame.setLocationRelativeTo(null);
		appFrame.setVisible(true);
	}

}
