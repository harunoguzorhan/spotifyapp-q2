package com.harunoguzorhan.q2;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.wrapper.spotify.models.Track;

public class AppFrame extends JFrame {

	JTextField searchTextField = new JTextField();
	JButton searchButton = new JButton("search");
	JButton tagButton = new JButton("tag");
	List<MyTrack> myTracks = new ArrayList<MyTrack>();

	JPanel tracksPanel = new JPanel();

	public AppFrame() {
		setLayout(new FlowLayout());
		setTitle("spotify app");
		searchTextField.setSize(100, 20);
		searchTextField.setText("Enter a keyword to search...");
		add(searchTextField);
		add(searchButton);
		add(tagButton);

		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				List<Track> tracks = SpotifyApi
						.searchTracksByKeyword(searchTextField.getText());
				tracksPanel.setLayout(new GridLayout(tracks.size(), 1));
				for (int i = 0; i < tracks.size(); i++) {
					Track track = tracks.get(i);
					MyTrack myTrack = new MyTrack();
					myTrack.setTrack(track);
					tracksPanel.add(myTrack.getCheckBox());
					myTracks.add(myTrack);
				}
				add(tracksPanel);
				pack();
				double x = getSize().getHeight();
				int y = (int) x;
				setSize(240, y);
			}
		});

		tagButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String tag = JOptionPane.showInputDialog("Please write a tag");
				addTagToSelectedTracks(tag);

			}

		});

	}

	private void addTagToSelectedTracks(String tag) {
		for (MyTrack myTrack : myTracks) {
			if (myTrack.getCheckBox().isSelected()) {
				if (!myTrack.addTag(tag)) {
					JOptionPane.showMessageDialog(null, "Error for tagging "
							+ myTrack.getTrack().getName()
							+ "! You cant tag a track with more than "
							+ AppProperties.TAG_NUMBER, "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}

	}

}
