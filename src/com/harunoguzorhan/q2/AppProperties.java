package com.harunoguzorhan.q2;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class AppProperties {

	public static String SPOTIFY_CLIENT_ID = "";
	public static String SPOTIFY_SECRET_KEY = "";
	public static int TAG_NUMBER = -1;
		

	public static Properties getProperties() {
		Properties properties = new Properties();
		try {
			String propertiesFileName = "conf\\app.properties";
			File file = new File(propertiesFileName);

			FileInputStream fis = new FileInputStream(file);
			properties = new Properties();
			properties.load(fis);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return properties;
	}

	public static void loadProperties() {
		try {
			Properties p = AppProperties.getProperties();
			SPOTIFY_CLIENT_ID = p.getProperty("SPOTIFY_CLIENT_ID");
			SPOTIFY_SECRET_KEY = p.getProperty("SPOTIFY_SECRET_KEY");
			TAG_NUMBER = Integer.parseInt(p
					.getProperty("TAG_NUMBER"));
						
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
