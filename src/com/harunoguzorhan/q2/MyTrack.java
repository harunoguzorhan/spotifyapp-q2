package com.harunoguzorhan.q2;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

import com.wrapper.spotify.models.Track;

public class MyTrack {
	private Track track;
	private List<String> tags = new ArrayList<String>();
	private JCheckBox checkBox;

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
		this.checkBox = new JCheckBox();
		checkBox.setText(track.getName());
	}

	public List<String> getTags() {
		return tags;
	}

	public boolean addTag(String tag) {
		if (tags.size() < AppProperties.TAG_NUMBER) {
			tags.add(tag);
			return true;
		}
		return false;
	}

	public JCheckBox getCheckBox() {
		return checkBox;
	}

	public void setCheckBox(JCheckBox checkBox) {
		this.checkBox = checkBox;
	}
}
